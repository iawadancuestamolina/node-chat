const express = require('express');
const router = express.Router();
const path = require('path');
const user = require('../controllers/user');


//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('chatList'));
});

router.post('/create', user.add);
router.post('/create', function(req,res){
  res.sendFile(path.resolve('historial.html'));
});

// router.post('/user', function(req, res){
//   var user = new User({name: req.body.name, age: req.body.age});
//   user.save();
//   console.log(user);
//   return user;
// });

module.exports = router;